# RENDU DU PROJET GIT Administration et gestion de code
```
    Auteur : Jérémie Kahan
    
    Contenu : Module de calcul arithmétique simple avec sa batterie de tests assorciée
    
    Résultat : Module fonctionnel, tests fonctionnels, mise en package validée, passage au crible par Pylint optimisé.

```
Sources éventuelles : M. Fabrice Jumel

https://gitlab.com/fabricejumel/tp1_ex8v0



# Daté du 01/04/2020